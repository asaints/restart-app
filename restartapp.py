# Python version: 2.7

'''
	This script checks if an application served by node.js is down (not responding with a 200) and runs the commands to restart the node.js server when needed. 
'''

import time
import urllib
import os
import sys
import re
import socket

def main():
	mapping = {
		"development": {
			"url": <OMITED> #development URL of the app to check
			"nodejspath": <OMITED> #file path of node.js file that serves development env UI
		},

		"production": {
			"url": <OMITED> #production URL of the app to check
			"nodejspath": <OMITED> #file path of node.js file that serves production env UI
		}
	}

	env = "development" #['development', 'production']

	mapping["production"]["url"] = (mapping["production"]["url"] if prod2check() == False else <OMITED SERVER 2 URL>) #reassigns the production url if detected that server2 is hosting

	try:
		statuscode = urllib.urlopen(mapping[env]['url']).getcode() # try to access application URL. If the request has no response, run exception clause

	except IOError as e:
		restartNode()

	#if response statuscode is anything other than 200
	if statuscode != int(200): 
		restartNode()

def prod2check():
	'''function to check of the hosting server is the second server'''

	hostname = socket.gethostname().lower() #get server hostname
	prod2url = <OMITED>.lower() #production server 2 url

	if (len(re.findall(prod2url, hostname)) > 0):
		return True

	else:
		return False

def restartNode():
	'''function that executes the shell commands to restart node.js'''

	command = 'nohup node ' + mapping[env]['nodejspath']
	logmessage = mapping[env]['url'] + " detected down. Node.js server restarted."

	logAction(logmessage)

	os.system(command)
	sys.exit()

def logAction(message):
	'''function that logs restart incidents by appending to a text file'''

	f = open(<OMITED>, "a+")

	current_time = time.localtime()
	today = time.strftime("%m\%d\%Y %H:%M:%S", current_time)
	log = today + ": " + message + "\n"

	f.write(log)
	f.close()

if __name__ == "__main__":
	main()